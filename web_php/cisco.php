<!DOCTYPE html>
<html>
<h1><span>DADAFECH</span> Cursos</h1>
<link rel="stylesheet" type="text/css" href="./CSS/modelo-curso.css">
<title>Curso Cisco</title>
<link rel="icon" href="./img/Logo.png">

<head>

  <meta charset="UTF-8">
</head>

<header>
     <a href="index.html"> <img class="logo" src="./img/Logo.png"
          width="70"
          height="80"></a>
     
     <nav>
     <ul> 
      <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
      <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
      <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
      <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
      <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
      <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
    </ul>
     
     </nav>
     </header>
<body>
    <div class="general">
        <div class="imagen">
            <img src="./img/cisco.jpg" alt="" width="400" height="400">
            <a class="boton_personalizado" href="#">Agregar al carrito</a>

        </div>

        <div class="titulo">
            <h1> CISCO CCNA 200-301 EN ESPAÑOL </h1>
            <p>Curso para la Certificación Cisco CCNA 200-301 en español. Aprende sobre redes con equipos Cisco de forma fácil. </p> <br><br>
            <P>PRECIO: 300€ </P><br><br>
            <p><meter class="nivel" min="25" max="100"
              low="25" high="60"
              optimum="23" value="70"></meter> Nivel medio </p>
        </div>
    </div>
    <div class="container">
        <div class="lbl-menu">
            <label for="radio1">DESCRIPCIÓN</label>
            <label for="radio2">REQUISITOS</label>
            <label for="radio3">DURACIÓN</label>
            <label for="radio4">PROFESOR</label>
            <label for="radio5">CONTENIDOS</label>

        </div>
        
        <div class="content">
           
            <input type="radio" name="radio" id="radio1" checked>
            <div class="tab1">
                <h2>Descripción</h2>
                <p>Este curso se enfoca en proporcionar a los estudiantes las habilidades y el conocimiento para entender el funcionamiento de las redes, así como la teoría y práctica como base para la certificación de Cisco CCNA en Routing and Switching.

                  En este curso se cubren todos los tópicos que necesitas saber sobre CCNA. Es una excelente guía tanto para las personas que se inician en el tema de Redes con equipos Cisco o bien para aquellos que ya tienen algunos conocimientos y desean repasar algunos temas importantes.
                  
                  El estudiante tendrá una buena base teórica y además podrá realizar prácticas guiadas basadas en los laboratorios que se presentan en el curso.
                  
                  Así que si desea iniciarte en el tema de redes, este es un excelente comienzo ya que tendrás acceso al curso en video y además podrás realizar las consultas que tengas al instructor utilizando mensajes privados.
                  
                  Este curso tiene como objetivo adicional, que el estudiante pueda aplicar los conocimientos adquiridos en la presentación de su examen de certificación y en su vida laboral.
                  
                  Hemos puesto nuestro esfuerzo en este curso y continuaremos mejorándolo para su mejor aprovechamiento.</p><br>
                  
                  Nota: Todas las marcas o derechos de cualquier tipo, nombres registrados, logos o insignias, usados o citados en este curso, son propiedad de sus respectivos dueños. El nombre CISCO y/o Packet Tracer o cualquier otro nombre mencionado se utiliza como referencia del fabricante.
                  </p>            
			</div>
            
            <input type="radio" name="radio" id="radio2">
            <div class="tab2">
                <h2>Requisitos</h2>
                <p>Muchas ganas de aprender y pasion por la tecnologia
                </p>
            </div>
            
            <input type="radio" name="radio" id="radio3">
            <div class="tab3">
                <h2>Duración</h2>
                <p>Este curso dura 90h</p>
            </div>
            
            <input type="radio" name="radio" id="radio4">
            <div class="tab4">
                <h2>Lorenzo Real Rodriguez</h2>
                <p><img class="profe" src="./img/profe1.jpg" width="90" height="90"></p>
                 <p> Soy diseñador y programador web desde hace más de 15 años.

                  No te aburriré ni te contaré la tipica historia de que soy programador desde los 6 años porque eso sería mentir.

                Empecé a los 18 años y la programación cambió mi vida.
                </p>
            </div>

            <input type="radio" name="radio" id="radio5">
            <div class="tab5">
                <h2>Contenidos</h2>
                <p>Fundamentos: Roles y funciones de los componentes  30h</p>
                <p>Direccionamiento IPv6  30h</p>
                 <p>  Routing  30h </p>
            </div>
        </div>
    </div>
    <div class="container1"></div>
    <footer>
      <!-- Footer main -->
      <section class="ft-main">
        <div class="ft-main-item">
          <h2 class="ft-title">Regístrate</h2>
          <ul>
            <li><a href="registro.html">Regístrate</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Contacta con nosotros</h2>
          <ul>
            <li><a href="contactos.html">Contacto</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Suscríbete ahora</h2>
          <p>Suscríbete para no perderte nada</p>
          <form method="post">
            <input type="email" name="email" placeholder="Dirección email">
            <input type="submit" value="SUSCRIBETE">
          </form>
        </div>

        <div class="social">
            <a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
            <a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
            <a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
            <a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
       </div>
      </section>
    
    </footer>
 </html>