<!DOCTYPE html>
<html>
<h1><span>DADAFECH</span> Cursos</h1>
<link rel="stylesheet" type="text/css" href="./CSS/modelo-curso.css">
<title>Curso Packet Tracer</title>
<link rel="icon" href="./img/Logo.png">


<head>

  <meta charset="UTF-8">
</head>

<header>
     <a href="index.html"> <img class="logo" src="./img/Logo.png"
          width="70"
          height="80"></a>
     
     <nav>
     <ul> 
      <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
      <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
      <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
      <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
      <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
      <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
    </ul>
     </nav>
     </header>
<body>
    <div class="general">
        <div class="imagen">
            <img src="./img/pt.webp" alt="" width="400" height="400">
            <a class="boton_personalizado" href="#">Agregar al carrito</a>

        </div>

        <div class="titulo">
            <h1> GUIA BÁSICA DE CISCO PACKET TRACER PARA EL CCNA   </h1>
            <p>¿Estás listo para el examen CCNA? Práctica y Certificate preparando laboratorios con el potente simulador de redes Cisco
            </p> <br><br>
            <P>PRECIO: 300€ </P><br><br>
            <p><meter class="nivel" min="25" max="100"
              low="75" high="25"
              optimum="60" value="100"></meter> Nivel alto </p>
        </div>
    </div>
    <div class="container">
        <div class="lbl-menu">
            <label for="radio1">DESCRIPCIÓN</label>
            <label for="radio2">REQUISITOS</label>
            <label for="radio3">DURACIÓN</label>
            <label for="radio4">PROFESOR</label>
            <label for="radio5">CONTENIDOS</label>

        </div>
        
        <div class="content">
           
            <input type="radio" name="radio" id="radio1" checked>
            <div class="tab1">
                <h2>Descripción</h2>
                <p>¿Estás listo para el examen CCNA?</p><br>

                <p> ¿Te has preparado los suficiente con prácticas de laboratorio?</p> <br>
                  
                <p> No corras riesgos y prepárate aprendiendo la magia de Cisco Packet Tracer</p> <br>
                  
                 <p>Aprende a usar el potente simulador de redes Cisco Packet Tracer y revisa en detalle el comportamiento de las redes, como se configuran cada uno de los elementos de una red, como interactúan los protocolos, como es el funcionamiento de equipos clientes,  Switch, Router, Servidores, como se configuran servicios de Red como DNS, DHCP y Web.</p>
                  
                  Iniciamos con la introducción del uso de Cisco Packet Tracer 6.2 (valido para 7.0, 7.1 y 7.2), revisamos los entornos de trabajo y sus modos de funcionamiento.
                  
                  <p>Realizaremos prácticas y laboratorios en la configuración de redes, podremos revisar y validar conceptos, y sistemas de redes técnicos y complejos, por medio de la herramienta Cisco Packet Tracer.</p><br>
                  
                  Con este curso podrás prepararte para rendir los exámenes de Cisco CCNA y CCNP, ya que podrás contar con el conocimiento y práctica de esta vital herramienta de simulación de redes.
                  
                  </p>            
			</div>
            
            <input type="radio" name="radio" id="radio2">
            <div class="tab2">
                <h2>Requisitos</h2>
                <p>Es deseable que descargues Cisco Packet Tracer</p>
              <p>  Es deseable conocimientos básicos de redes Cisco</p>
                <p> Muchas ganas de seguir aprendiendo :)</p>
            </div>
            
            <input type="radio" name="radio" id="radio3">
            <div class="tab3">
                <h2>Duración</h2>
                <p>Este curso dura 90h</p>
            </div>
            
            <input type="radio" name="radio" id="radio4">
            <div class="tab4">
                <h2>Félix Moreno Bien</h2>
                <p><img class="profe" src="./img/profe3.jpg" width="90" height="90"></p>
                 <p> Ingeniero de Redes con más de 20 años de experiencia en informática, servidores y redes de datos de distintos tamaños.Experiencia en grandes empresas de servicios y en la gran Minería.
                </p>
            </div>

            <input type="radio" name="radio" id="radio5">
            <div class="tab5">
              <h2>Contenidos</h2>
              <p>Empezando 30h</p>
              <p>Configuración de dispositivos  30h</p>
               <p> Practtica con Packet Tracer  30h </p>
            </div>
        </div>
    </div>
    <div class="container1"></div>
    <footer>
      <!-- Footer main -->
      <section class="ft-main">
        <div class="ft-main-item">
          <h2 class="ft-title">Regístrate</h2>
          <ul>
            <li><a href="registro.html">Regístrate</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Contacta con nosotros</h2>
          <ul>
            <li><a href="contactos.html">Contacto</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Suscríbete ahora</h2>
          <p>Suscríbete para no perderte nada</p>
          <form method="post">
            <input type="email" name="email" placeholder="Dirección email">
            <input type="submit" value="SUSCRIBETE">
          </form>
        </div>

        <div class="social">
            <a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
            <a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
            <a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
            <a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
       </div>
      </section>
    
    </footer>
 </html>