<?php
include("config.php");

// 2. isset() del boton login
if(isset($_POST['login'])){

    // 3. Variables $_POST[]
    $u = $_POST['usuario'];
    $c = $_POST['clave']; 

    if($u == "" || $_POST['clave'] == null){ // Validamos que ningún campo quede vacío
        echo "<script>alert('Error: usuario y/o clave vacios!!');</script>"; // Se utiliza Javascript dentro de PHP
    }else{
        // 4. Cadena de SQL
        $sql = "SELECT * FROM alumnos WHERE nombre = '$u' AND clave = '$c'";
        echo $sql;

        // 5. Ejecuto cadena query()
        if(!$consulta = $conexion->query($sql)){
            echo "ERROR: no se pudo ejecutar la consulta!";
        }else{
            // 6. Cuento registros obtenidos del select. 
            // Como el nombre de usuario en la clave primaria no debería de haber mas de un registro con el mismo nombre.
            $filas = mysqli_num_rows($consulta);

            // 7. Comparo cantidad de registros encontrados
            if($filas == 0){
                echo "<script>alert('Error: usuario y/o clave incorrectos!!');</script>";
            }else{
                header('location:datos_usuarios.php'); // Si está todo correcto redirigimos a otra página
            }

        }
    }
    
}
?>

<!DOCTYPE html>
<html lang="es">

<h1><span>DADAFECH</span> Cursos</h1>
<title>Login</title>
<link rel="icon" href="./img/Logo.png">
<link rel="stylesheet" type="text/css" href="./CSS/login.css">
<link rel="stylesheet" type="text/css" href="./CSS/style.css">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.css"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
    <title>Login</title>
</head>



<header>
    <a href="index.php"> <img class="logo" src="./img/Logo.png"
         width="70"
         height="80"></a>
    
        <nav>
        <ul> 
          <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
          <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
          <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
          <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
          <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
          <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
        </ul>
        </nav>
    </header>


<body>
    
  <div class="login_principal">
    <div class="card">
        <h1>Iniciar sesión</h1>
          <form method="post" name="logeo" action="datos_usuarios.php ">
            <input type="text" name="usuario" placeholder="Usuario">
            <div class="passwordInput">
                <input id="inputPass" type="password" name="clave" placeholder="Contraseña" />
                <span id="showPassword" class="material-icons" onclick="showPassword()">
                visibility
              </span>
                <span id="hidePassword" class="material-icons" onclick="hidePassword()">
                  visibility_off
              </span>
            </div>
              <input class="submit1" type="submit" name="login" value="Ingresar">
        </form>
    </div>
  </div>

    <footer>
        <!-- Footer main -->
        <section class="ft-main">
          <div class="ft-main-item">
          <h2 class="ft-title">Regístrate</h2>
          <ul>
            <li><a href="registro.html">Regístrate</a></li>
          </ul>
        </div>
          <div class="ft-main-item">
            <h2 class="ft-title">Contacta con nosotros</h2>
            <ul>
              <li><a href="contactos.html">Contacto</a></li>
            </ul>
          </div>
          <div class="ft-main-item">
            <h2 class="ft-title">Suscríbete ahora</h2>
            <p>Suscríbete para no perderte nada</p>
            <form class="form1" method="post">
              <input class="email1" type="email" name="email" placeholder="Dirección email">
              <input class="submit1" type="submit" value="SUSCRIBETE">
            </form>
          </div>
  
          <div class="social">
              <a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
              <a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
              <a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
              <a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
         </div>
        </section>     
      </footer>
</body>

<script>
    function showPassword() {
        document.getElementById('inputPass').type = "text";
        document.getElementById('hidePassword').style.display = 'inline';
        document.getElementById('showPassword').style.display = 'none';
    }

    function hidePassword() {
        document.getElementById('inputPass').type = "password"
        document.getElementById('hidePassword').style.display = 'none';
        document.getElementById('showPassword').style.display = 'inline';
    }
</script>

</html>