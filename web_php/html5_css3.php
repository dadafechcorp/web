<!DOCTYPE html>
<html>
<h1><span>DADAFECH</span> Cursos</h1>
<link rel="stylesheet" type="text/css" href="./CSS/modelo-curso.css">
<title>Curso Html y CSS</title>
<link rel="icon" href="./img/Logo.png">

<head>

  <meta charset="UTF-8">
</head>

<header>
     <a href="index.html"> <img class="logo" src="./img/Logo.png"
          width="70"
          height="80"></a>
     
     <nav>
     <ul> 
      <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
      <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
      <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
      <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
      <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
      <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
    </ul>
     
     </nav>
     </header>
<body>
    <div class="general">
        <div class="imagen">
            <img src="./img/html5-css3-js.png" alt="" width="400" height="400">
            <a class="boton_personalizado" href="#">Agregar al carrito</a>

        </div>

        <div class="titulo">
            <h1> DESARROLLO WEB DESDE CERO: HTML5, CSS3, JAVASCRIPT    </h1>
            <p>Aprende los lenguajes que dan vida a la web desde cero a un nivel avanzado. Aprenderás HTML5, CSS3, Javascript.</p> <br><br>
            <P>PRECIO: 70€ </P><br><br>
            <p><meter class="nivel" min="25" max="100"
                low="25" high="60"
                optimum="23" value="70"></meter> Nivel medio </p>
        </div>
    </div>
    <div class="container">
        <div class="lbl-menu">
            <label for="radio1">DESCRIPCIÓN</label>
            <label for="radio2">REQUISITOS</label>
            <label for="radio3">DURACIÓN</label>
            <label for="radio4">PROFESOR</label>
            <label for="radio5">CONTENIDOS</label>

        </div>
        
        <div class="content">
           
            <input type="radio" name="radio" id="radio1" checked>
            <div class="tab1">
                <h2>Descripción</h2>
                <p>¿Quieres iniciar en el mundo del desarrollo web y no sabes el camino a seguir?
                </p><br>

                <p>Cuando iniciamos en este mundo no se sabe el camino que se debe elegir. Es el problema principal de quien quiere iniciar en el desarrollo web, por tal motivo, creé este curso, para darte los tips y consejos necesarios y puedas adentrarte en el mundo del desarrollo web.  </p> <br>
                  
                <p>La linea de aprendizaje creada en este curso es ideal para una persona que no tenga ningún conocimiento previo, no importa si no sabes nada sobre la programación; iremos desde los conceptos más básicos y lograremos, con conceptos y prácticas, que tú puedas desarrollar cualquier página web. La metodología de aprendizaje es la ideal:        </p> <br>
                  
                 <p>Cada concepto explicado desde cero, paso a paso.

                    Casi 20 horas de contenido.
                    
                    Breve teoría y muchos ejercicios prácticos.
                    
                    Linea de aprendizaje y orden progresivo desarrollado con detalle para que cada vez que avances de nivel sientas la progresión del curso.
                    
                    Lo que aprendiste no lo olvidarás; por el contrario en cada sección aplicaremos más ejercicios para que refresques lo aprendido y lo pongas en práctica.
                    
                    Pruebas de contenido al finalizar cada sección que refrescaran y agudizarán tus conocimientos.
                    
                    
                    
                    Desarrollo web desde cero abarca muchísimo,es empezar desde conceptos básicos hasta crear tu propia página web.
                    
                    Nuestra primera sección iremos desde el concepto más básico del desarrollo web y abarcaremos cada una de las tecnologías, te introducirás en el mundo de la programación.
                    
                    La estructura de nuestra web es importante, por tal motivo comenzaremos a darle estructura y contenido a nuestras  páginas web aprendiendo el lenguaje de marcado HTML5.
                    
                    Al dominar nuestro contenido y estructura, empezará la magia; porque eso es CSS3, magia pura para aplicarle presentación y estilos a nuestras páginas web; lo que imagines, lo puedes hacer con CSS3.
                    
                    Nuestra evolución en este punto, es adaptar todo lo que sabemos al mundo actual; los diferentes tamaños de pantalla. Actualmente, tenemos una gran variedad de dispositivos con diferentes tamaños de pantalla, y queremos que nuestros elementos se adapten con el Responsive Desing.
                    
                    Ya nuestras web tienen estructuras y estilos, pero les falta vida; para eso llega nuestro Javascript, el que es considerado el mejor lenguaje y el que mayor oportunidades laborales ofrece, ademas de que su sintaxis y fácil comprensión lo hacen ideal como nuestro primer lenguaje de programación.
                    
                    A este punto, con nuestros conocimientos adquiridos, ya podemos realizar cualquier página web y lo pondremos a prueba desarrollando desde cero una página web para una empresa de comida rápida.
                    
                    Ademas, tendrás soporte y ayuda para que puedas comprender todos los conceptos explicados y siempre tendrás una comunicación directa para resolver tus dudas.
                    
                    
                    
                    Esto es más que un curso, es el inicio de una carrera en el desarrollo web; vas a aprender lo que se necesita para crear cualquier página web.
                    
                    Únete al curso, te estoy esperando; ¡iniciemos el camino del desarrollo web!</p>
                    
            </div>
            
            <input type="radio" name="radio" id="radio2">
            <div class="tab2">
                <h2>Requisitos</h2>
                <p>No se necesitan conocimientos previos.</p>
                <p> Una computadora con conexión a internet</p>
                    
            </div>
            
            <input type="radio" name="radio" id="radio3">
            <div class="tab3">
                <h2>Duración</h2>
                <p>Este curso dura 90h</p>
            </div>
            
            <input type="radio" name="radio" id="radio4">
            <div class="tab4">
                <h2>Maria Prado Gago</h2>
                <p><img class="profe" src="./img/profe5.jpg" width="90" height="90"></p>
                 <p> Directora del área de Programación Web en Dadafech .</p>
            </div>

            <input type="radio" name="radio" id="radio5">
            <div class="tab5">
              <h2>Contenidos</h2>
              <p>HTML5: Estructura y contenido  30h</p>
              <p>CSS·: Presentación y estilos  30h</p>
               <p>Responsive Desing: Adaptable a dispositivos móviles   30h </p>
            </div>
        </div>
    </div>
    <div class="container1"></div>
    <footer>
      <!-- Footer main -->
      <section class="ft-main">
        <div class="ft-main-item">
          <h2 class="ft-title">Regístrate</h2>
          <ul>
            <li><a href="registro.html">Regístrate</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Contacta con nosotros</h2>
          <ul>
            <li><a href="contactos.html">Contacto</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Suscríbete ahora</h2>
          <p>Suscríbete para no perderte nada</p>
          <form method="post">
            <input type="email" name="email" placeholder="Dirección email">
            <input type="submit" value="SUSCRIBETE">
          </form>
        </div>

        <div class="social">
            <a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
            <a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
            <a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
            <a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
       </div>
      </section>
    
    </footer>
 </html>