<!DOCTYPE html>
<html>
<h1><span>DADAFECH</span> Cursos</h1>
<link rel="stylesheet" type="text/css" href="./CSS/cursos.css">
<title>Cursos</title>
<link rel="icon" href="./img/Logo.png">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="css/estilos.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">

	<script src="js/jquery-3.2.1.js"></script>
	<script src="js/script.js"></script>
	<script src="https://kit.fontawesome.com/424a276fe5.js" crossorigin="lupita"></script>

</head>
<header>
	<a href="index.html"> <img class="logo" src="./img/Logo.png"
		 width="70"
		 height="80"></a>
	
	<nav>
     <ul> 
      <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
      <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
      <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
      <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
      <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
      <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
    </ul>
	
	</nav>
	</header>
	<div class="box">
		<div class="container-3">
			<span class="icon"><i class="fa fa-search"></i></span>
			<input type="search" id="search" placeholder="Busqueda..." />
		</div>
	  </div>

<body>
	
	<div class="wrap">
		<div class="nombre_cat">
			<h1>CATEGORIAS</h1>
		</div>
		<h1>NUESTROS CURSOS</h1>

		<div class="store-wrapper">
			<div class="category_list">
				<a href="#" class="category_item" category="all"><b>TODO</b></a>
				<a href="#" class="category_item" category="redes"><b>REDES</b></a>
				<a href="#" class="category_item" category="programación"><b>PROGRAMACIÓN</b></a>
				<a href="#" class="category_item" category="desarrolo_web"><b>DESARROLLO WEB</b></a>
				<a href="#" class="category_item" category="bbdd"><b>BASES DE DATOS</b></a>


				
			</div>
			<section class="products-list">
				<div class="product-item" category="redes">
					<a href="cisco.php"><img src="./img/cisco1.jpg" alt="" width="300px" height="300px" >
						<h3>Cisco CCNA 200-301 en español </h3>
						<h2>300€</h2>
					<meter class="nivel" min="25" max="100"
            		low="25" high="60"
           			optimum="23" value="70"></meter> Nivel medio <br><br></a>
				</div>
				<div class="product-item" category="desarrolo_web">
					<a href="kubernetes.php"><img src="./img/kubernetes1.jpg" alt="" width="300px" height="300px">
					 <h3>Kubernetes, de principiante a experto </h3>
						<h2>180€</h2>
						<meter class="nivel" min="25" max="100"
            			low="25" high="75"
           				optimum="60" value="40"></meter> Nivel bajo <br><br>
					</a>
				</div>
				<div class="product-item" category="redes">
					<a href="pt.php"><img src="./img/packettracer.png" alt="" width="300px" height="300px" >
					<h3>Guia básica de Cisco PT para el CCNA </h3>
						<h2>240€</h2>
						<meter class="nivel" min="25" max="100"
         				 low="75" high="25"
         				 optimum="60" value="100"></meter> Nivel alto <br><br></a>
				</div>
				
				<div class="product-item" category="programación">
					<a href="c++.php"><img src="./img/c++.jpg" alt="" width="300px" height="300px" >
					<h3>PROGRAMACIÓN EN C DE CERO A EXPERTO CON ESTRUCTURAS DE DATOS </h3>
						<h2>135€</h2>
						<meter class="nivel" min="25" max="100"
          low="75" high="25"
          optimum="60" value="100"></meter> Nivel alto<br><br></a>
				</div>
				<div class="product-item" category="desarrolo_web">
					<a href="html5_css3.php"><img src="./img/html5-css3-js.png" alt="" width="300px" height="300px">
						<h3>DESARROLLO WEB DESDE CERO: HTML5, CSS3, JAVASCRIPT </h3>
						<h2>75€</h2>
						<meter class="nivel" min="25" max="100"
						low="25" high="60"
						   optimum="23" value="70"></meter> Nivel medio<br><br></a>
				</div>
				<div class="product-item" category="bbdd">
					<a href="php.php"><img src="./img/php_mysql.jpg" alt="" width="300px" height="300px">
						<h3>APRENDE PHP MYSQL Y SERVIDOR WEB CURSO PRÁCTICO DESDE 0  </h3>
						<h2>90€</h2>
						<meter class="nivel" min="25" max="100"
          low="75" high="25"
          optimum="60" value="100"></meter> Nivel alto<br><br></a>
				</div>
			</section>
		</div>
	</div>

	<div class="container"></div>
	<footer>
	  <!-- Footer main -->
	  <section class="ft-main">
		<div class="ft-main-item">
          <h2 class="ft-title">Regístrate</h2>
          <ul>
            <li><a href="registro.html">Regístrate</a></li>
          </ul>
        </div>
		<div class="ft-main-item">
		  <h2 class="ft-title">Contacta con nosotros</h2>
		  <ul>
			<li><a href="contactos.html">Contacto</a></li>
		  </ul>
		</div>
		<div class="ft-main-item">
		  <h2 class="ft-title">Suscríbete ahora</h2>
		  <p>Suscríbete para no perderte nada</p>
		  <form method="post">
			<input type="email" name="email" placeholder="Dirección email">
			<input type="submit" value="SUSCRIBETE">
		  </form>
		</div>

		<div class="social">
			<a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
			<a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
			<a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
			<a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
	   </div>
	  </section>
	
	</footer>
	</body>
	</html>