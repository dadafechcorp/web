
<!DOCTYPE html>
<html>
<h1><span>DADAFECH</span> Cursos</h1>
<link rel="stylesheet" type="text/css" href="./CSS/style.css">
<title> Inicio</title>
<link rel="icon" href="./img/Logo.png">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Material+Icons" rel="stylesheet">
</head>




<header>
<a href="index.html"> <img class="logo" src="./img/Logo.png"
     width="70"
     height="80"></a>

    <nav>
    <ul> 
      <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
      <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
      <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
      <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
      <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
      <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
    </ul>
    </nav>
</header>



<body>
  

    <div class="principal">
          <div class="slider">
              <div class="slide" id="first">
                <h3>Curso Programación</h3>
              </div>
              <div class="slide" id="second">
                  <h3>Desarrollo Web</h3>
              </div>
              <div class="slide" id="third">
                  <h3>HTML CSS JAVASCRIPT</h3>
              </div>
              <div class="slide" id="fourth">
                  <h3>REDES CISCO CCNA</h3>
              </div>
                    <button id="check-left" onclick="moveSlide(1)">
                      <span class="material-icons">
                        chevron_left
                      </span>
                    </button>
                    <button id="check-right" onclick="moveSlide(-1)">
                      <span class="material-icons">
                        chevron_right
                      </span>
                    </button> 
          </div>
  
      <div class="main">
          <div class="ofertas">
              <h1 class="titulo">Últimas Ofertas</h1>
              <hr>

              <a href="cisco.html"><img class="foto1" src="./img/cisco.jpg" width="150" height="150"></a>
              <small class="foto1"><br><b>CISCO CCNA 200-301 EN ESPAÑOL</b></small>
              <br><br><br>
              <p><b>100€ </b></p> <br>

              <hr>
              <a href="kubernetes.html"><img  src="./img/kubernetes.png" width="150" height="150"></a>
              <small><br><b>KUBERNETES, DE PRINCIPIANTE A EXPERTO</b></small>
              <br><br><br>
              <p><b>50€</b></p>
          </div>

          <div class="tematicas">
              <div class="programacion">
                  <h2>Programación</h2> <br><br>
                  <img src="./img/programacion.jpg" alt="" width="300" height="300">
              </div>

              <div class="desarrollo">
                <H2>Desarrollo web</H2> <br><br>
                <img src="./img/desarrollo.jpg" alt="" width="300" height="300">
              </div>

              <div class="redes">
                  <H2>Redes</H2> <br><br>
                  <img src="./img/redes.jpg" alt="" width="300" height="300">
              </div>

              <div class="bbdd">
                <H2>Bases de datos</H2> <br><br>
                <img src="./img/bbdd.jpg" alt="" width="300" height="300">
              </div>
          </div>    
                
        </div>
      </div>       
    
     
     <!-- Footer main -->
    <footer>       
       <section class="ft-main">
         <div class="ft-main-item">
          <h2 class="ft-title">Regístrate</h2>
          <ul>
            <li><a href="registro.html">Regístrate</a></li>
          </ul>
        </div>
         <div class="ft-main-item">
           <h2 class="ft-title">Contacta con nosotros</h2>
           <ul>
             <li><a href="contactos.html">Contacto</a></li>
           </ul>
         </div>
         <div class="ft-main-item">
           <h2 class="ft-title">Suscríbete ahora</h2>
           <p>Suscríbete para no perderte nada</p>
           <form method="post">
             <input type="email" name="email" placeholder="Dirección email">
             <input type="submit" value="SUSCRIBETE">
           </form>
         </div>
 
         <div class="social">
             <a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
             <a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
             <a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
             <a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
        </div>
       </section>     
    </footer>     
</body>


    <script>
        let position = 0;

        function moveSlide(direction) {
            const newPosition = position + direction;
            position = newPosition < -3 ? 0 : newPosition == 1 ? -2 : newPosition;

            const slides = document.getElementsByClassName('slide');
            Array.from(slides).forEach((slide) => {
                slide.style.transform = `translateX(calc(${100*position}% - ${4*Math.abs(position)}px))`;
            });
        }
    </script>
</html>