<!DOCTYPE html>
<html>
<h1><span>DADAFECH</span> Cursos</h1>
<link rel="stylesheet" type="text/css" href="./CSS/modelo-curso.css">
<title>Curso Kubernetes</title>
<link rel="icon" href="./img/Logo.png">

<head>

  <meta charset="UTF-8">
</head>

<header>
     <a href="index.html"> <img class="logo" src="./img/Logo.png"
          width="70"
          height="80"></a>
     
     <nav>
     <ul> 
      <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
      <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
      <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
      <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
      <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
      <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
    </ul>
     
     </nav>
     </header>
<body>
    <div class="general">
        <div class="imagen">
            <img src="./img/kubernetes.png" alt="" width="400" height="400">
            <a class="boton_personalizado" href="#">Agregar al carrito</a>

        </div>

        <div class="titulo">
            <h1>KUBERNETES, DE PRINCIPIANTE A EXPERTO  </h1>
            <p>¡Conviértete en un Kubernetes Máster dominando la herramienta más popular para administrar contenedores Docker en 2020! </p> <br><br>
            <P>PRECIO: 180€ </P><br><br>
            <p><meter class="nivel" min="25" max="100"
              low="25" high="75"
              optimum="60" value="40"></meter> Nivel bajo</p>
        </div>
    </div>
    <div class="container">
        <div class="lbl-menu">
            <label for="radio1">DESCRIPCIÓN</label>
            <label for="radio2">REQUISITOS</label>
            <label for="radio3">DURACIÓN</label>
            <label for="radio4">PROFESOR</label>
            <label for="radio5">CONTENIDOS</label>

        </div>
        
        <div class="content">
           
            <input type="radio" name="radio" id="radio1" checked>
            <div class="tab1">
                <h2>Descripción</h2>
                <p>Probablemente has escuchado la palabra "Kubernetes" más de una vez, y no tienes ni idea de qué es o cómo funciona. Sé cómo se siente, pero ¡Vamos a ponerle fin a eso! Con este curso, aprenderás Kubernetes de una vez por todas, y de la manera más sencilla posible.

                  No importa si eres un desarrollador, o administrador de sistemas, o DevOps. Con este curso, ¡Kubernetes está al alcance de todos!
                  
                  ¡No esperes más y aprende Kubernetes desde cero! Es una oportunidad única si quieres sobresalir en el mercado actual.
                  
                  Aprenderás desde las bases, hasta los conceptos más complicados y todo a un ritmo ágil y con mucha práctica.
                  
                  Dentro del temario veremos:
                  
                  <p>* Arquitectura</p>
                  
                  <p>  * Conceptos</p>
                  
                  <p>  * Pods</p>
                  
                  <p> * ReplicaSets</p>
                  
                  <p>  * Deployments</p>
                  
                  <p> * Limites</p>
                  
                  <p>  * Health Checks</p>
                  
                  <p> * Namespaces</p>
                  
                  <p> * PVC y PV</p>
                  
                  <p> * Ejercicios de código de aplicaciones del mundo real</p>
                  
                 <p> * Despliegues</p>
                  
                  <p>* ¡Y muchas cosas más!</p>
                  
                  ¡Ánimate! Es una excelente oportunidad.
                  
                  ¿Para quién es este curso?
                  DevOps Engineers que quieren aprender a orquestar contenedores
                  SysAdmins que desean convertir a contenedores sus aplicaciones
                  Desarrolladores que quieren incursar en automatización y contenedores
                  Cualquier persona con ganas de aprender Kubernetes!
                  </p>            
			</div>
            
            <input type="radio" name="radio" id="radio2">
            <div class="tab2">
                <h2>Requisitos</h2>
                <p>Debes comprender cómo funciona Docker</p>
                <p> Debes conocer qué es un contenedor</p>
                <p> Necesitas acceso a Internet y una máquina de más de 4GB de Ram</p>
            </div>
            
            <input type="radio" name="radio" id="radio3">
            <div class="tab3">
                <h2>Duración</h2>
                <p>Este curso dura 90h</p>
            </div>
            
            <input type="radio" name="radio" id="radio4">
            <div class="tab4">
                <h2>Danira Barron Ruesg</h2>
                <p><img class="profe" src="./img/profe2.jpg" width="90" height="90"></p>
                <p>Encuentro en Linux mi pasatiempo y diversión. Soy amante de las tecnologías open source y esto me ha llevado a integrar distintas herramientas proporcionando soluciones ágiles con Docker, Jenkins, Ansible, Git, etc. </p>
            </div>

            <input type="radio" name="radio" id="radio5">
            <div class="tab5">
              <h2>Contenidos</h2>
              <p>Arquitectura de Kubernetes - ¡Conoce todos los secretos!  30h</p>
              <p>ReplicaSets - Aprende a garantizar réplicas en tus Pods  30h</p>
               <p>Golang, JavaScript y Kubernetes - Aprende a construir aplicaciones reales 30h </p>
            </div>
        </div>
    </div>
    <div class="container1"></div>
    <footer>
      <!-- Footer main -->
      <section class="ft-main">
        <div class="ft-main-item">
          <h2 class="ft-title">Regístrate</h2>
          <ul>
            <li><a href="registro.html">Regístrate</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Contacta con nosotros</h2>
          <ul>
            <li><a href="contactos.html">Contacto</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Suscríbete ahora</h2>
          <p>Suscríbete para no perderte nada</p>
          <form method="post">
            <input type="email" name="email" placeholder="Dirección email">
            <input type="submit" value="SUSCRIBETE">
          </form>
        </div>

        <div class="social">
            <a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
            <a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
            <a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
            <a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
       </div>
      </section>
    
    </footer>
 </html>