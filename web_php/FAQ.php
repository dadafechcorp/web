<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>contactos.html</title>
  <link rel="stylesheet" type="text/css" href="./CSS/style.css">
  <link rel="stylesheet" type="text/css" href="./CSS/FAQS.css">
</head>


  <h1><span>DADAFECH</span> Cursos</h1>

  <header>
    <a href="index.html"> <img class="logo" src="./img/Logo.png" width="70" height="80"></a>

    <nav>
        <ul> 
          <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
          <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
          <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
          <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
          <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
          <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
        </ul>

    </nav>
  </header>
<div class="general">
    <div class="accordionMenu">
      <input type="radio" name="trg1" id="acc1" checked="checked">
      <label for="acc1">Cómo descargar el certificado de finalización </label>
      <div class="content">
        <div class="inner">
          
         <p>Cuando completes un curso, recibirás un certificado de finalización.</p>

         <p>¿Cómo descargar el certificado de finalización?</p>
          
         <p>1. Cuando hayas completado todos los elementos del currículum de un curso, 
           aparecerá la opción de descargar el certificado.</p>

         <p>2. Hacemos click en descargar.</p>

         <p>3. Ya estaría descargado.</p>

        </div>
      </div>

      <input type="radio" name="trg1" id="acc2">
      <label for="acc2">Descarga de recursos de cursos</label>
      <div class="content">
        <div class="inner">
         
          Muchos instructores añaden recursos adicionales a sus clases, como PDF, 
          plantillas de diseño o código fuente, con el fin de mejorar la experiencia de aprendizaje del curso. 
          <p>Son recursos que puedes descargar rápidamente en tu equipo para verlos</p>
          
        </div>
      </div>

      <input type="radio" name="trg1" id="acc3">
      <label for="acc3">¿Qué incluyen los cursos?</label>
      <div class="content">
        <div class="inner">

          Los profesores crean, poseen y gestionan cada curso.
          <p>La base de cada curso son sus clases, que pueden incluir vídeos, diapositivas y texto, así como ejercicios a lo largo del aprendizaje.</p>

          <p>Además, los profesores pueden agregar recursos y diferentes tipos de actividades prácticas, como una manera de mejorar la experiencia de aprendizaje de los estudiantes.

        </div>
      </div>

      <input type="radio" name="trg1" id="acc4">
      <label for="acc4">¿Por qué no se carga el PDF? </label>
      <div class="content">
        <div class="inner">          
          <p>A veces, un documento PDF tarda un poco en cargarse.<p>
          Desde el panel del curso, comprueba cuántas páginas se incluyen en el documento. 
          Si el documento tiene varias páginas es probable que el gran tamaño del archivo esté afectando al tiempo de carga. 
          <p>Intenta actualizar la página para solucionar el problema.</p>        
        </div>
      </div>

      <input type="radio" name="trg1" id="acc5">
      <label for="acc5">¿Dónde puedo obtener ayuda?</label>
      <div class="content">
        <div class="inner">
          Si no obtiene respuesta a su duda, pongase en contacto con nosotros. 
        </div>
      </div>
    </div>
    <footer>
      <!-- Footer main -->
      <section class="ft-main">
        <div class="ft-main-item">
          <h2 class="ft-title">¿Quienes somos?</h2>
          <ul>
            <li><a href="#">Servicios</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Contacta con nosotros</h2>
          <ul>
            <li><a href="contactos.html">Contacto</a></li>
          </ul>
        </div>
  
        <div class="ft-main-item">
          <h2 class="ft-title">Suscríbete ahora</h2>
          <p>Suscríbete para no perderte nada</p> 
          <form>       
            <input type="email" name="email" placeholder="Dirección email">
            <input type="submit" value="SUSCRIBETE"> 
            </form>       
        </div>

        <div class="social">
            <a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
            <a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
            <a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
            <a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
       </div>
      </section>     
    </footer>
  </body>