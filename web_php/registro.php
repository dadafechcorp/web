<?php
include("config.php");

if(isset($_POST['registro'])){

$nombre = $_POST['nombre'];
$apellido1 = $_POST['apellido1'];
$apellido2 = $_POST['apellido2'];
$dni = $_POST['dni'];
$email = $_POST['correo'];
$direccion = $_POST['direccion'];
$localidad = $_POST['localidad'];
$provincia = $_POST['provincia'];
$cp = $_POST['cp'];
$telefono = $_POST['tel'];
$contrasena = $_POST['contrasena'];


  
$sql="INSERT INTO alumnos (nombre, primerapellido,segundoapellido, dni, email, direccion, localidad, provincia, cpostal, telefono, clave) VALUES ('$nombre', '$apellido1', '$apellido2', '$dni', '$email', '$direccion', '$localidad', '$provincia', '$cp', '$telefono', '$contrasena')";

if (mysqli_query($conexion, $sql)) {
      echo "Nuevo usuario creado ";
} else {
      echo "Error: " . $sql . "<br>" . mysqli_error($conexion);
}
mysqli_close($conexion);
  }
?>


<!DOCTYPE html>
<html>
<h1><span>DADAFECH</span> Cursos</h1>
<link rel="stylesheet" type="text/css" href="./CSS/registro.css">
<title>Registro</title>
<link rel="icon" href="./img/Logo.png">

<head>
  <meta charset="UTF-8">
</head>

<header>
<a href="index.html"> <img class="logo" src="./img/Logo.png"
     width="70"
     height="80"></a>

    <nav>
     <ul> 
      <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
      <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
      <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
      <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
      <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
      <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
    </ul>
    </nav>
</header>
        
    <body>
       
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" class="formulario" method="post" >
    
        <h1 class="formulario__titulo">Regístrate</h1>

        <input type="text" class="formulario__input" name="nombre" required="">
        <label for=""
        class="formulario__label">Nombre *</label>

        <input type="text" class="formulario__input" name="apellido1" required="" >
        <label for=""
        class="formulario__label">1º Apellido *</label>

        <input type="text" class="formulario__input" name="apellido2" required="">
        <label for=""
        class="formulario__label">2º Apellido *</label>

        <input type="text" class="formulario__input" name="dni" required="">
        <label for=""
        class="formulario__label">DNI *</label>

        <input type="tel" class="formulario__input" name="tel" required="">
        <label for=""
        class="formulario__label">Teléfono *</label>

        <input type="email" class="formulario__input" name="correo" required="">
        <label for=""
        class="formulario__label">Correo *</label>

        <input type="text" class="formulario__input" name="direccion" required="">
        <label for=""
        class="formulario__label">Dirección *</label>

        <input type="text" class="formulario__input" name="localidad" required="">
        <label for=""
        class="formulario__label">Ciudad *</label>

        <input type="text" class="formulario__input" name="provincia" required="">
        <label for=""
        class="formulario__label">Provincia *</label>

        <input type="text" class="formulario__input" name="cp" required="">
        <label for=""
        class="formulario__label">CP *</label>

        <input type="text" class="formulario__input" name="contrasena" required="">
        <label for=""
        class="formulario__label">Contraseña *</label>

      

        <button class="formulario__submit" name="registro" >Registrarse</button>
    </form>
    

        <script src="./js/registro.js"></script>

    <div class="container"></div>
    <footer>
      <!-- Footer main -->
      <section class="ft-main">
        <div class="ft-main-item">
          <h2 class="ft-title">Regístrate</h2>
          <ul>
            <li><a href="registro.html">Regístrate</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Contacta con nosotros</h2>
          <ul>
            <li><a href="contactos.html">Contacto</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Suscríbete ahora</h2>
          <p>Suscríbete para no perderte nada</p>
          <form class="form_2" method="post">
            <input class="email2" type="email" name="email" placeholder="Dirección email">
            <input type="submit" value="SUSCRIBETE">
          </form>
        </div>

        <div class="social">
            <a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
            <a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
            <a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
            <a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
       </div>
      </section>     
    </footer>
</body>

</html>
