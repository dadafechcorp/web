<!DOCTYPE html>
<html>
<h1><span>DADAFECH</span> Cursos</h1>
<link rel="stylesheet" type="text/css" href="./CSS/modelo-curso.css">
<title>Curso php</title>
<link rel="icon" href="./img/Logo.png">

<head>

  <meta charset="UTF-8">
</head>

<header>
     <a href="index.html"> <img class="logo" src="./img/Logo.png"
          width="70"
          height="80"></a>
     
     <nav>
     <ul> 
      <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
      <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
      <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
      <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
      <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
      <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
    </ul>
     </nav>
     </header>
<body>
    <div class="general">
        <div class="imagen">
            <img src="./img/php_mysql.jpg" alt="" width="400" height="350">
            <a class="boton_personalizado" href="#">Agregar al carrito</a>

        </div>

        <div class="titulo">
            <h1> APRENDE PHP MYSQL Y SERVIDOR WEB: CURSO PRÁCTICO DESDE 0 </h1>
            <p>Aprende a Crear de Aplicaciones y Sitios Web, para que puedas Visualizarlos en Todo el Mundo desde (Pc, Tablet y Móvil).</p> <br><br>
            <P>PRECIO: 90€ </P><br><br>
            <p><meter class="nivel" min="25" max="100"
              low="75" high="25"
              optimum="60" value="100"></meter> Nivel alto </p>
        </div>
    </div>
    <div class="container">
        <div class="lbl-menu">
            <label for="radio1">DESCRIPCIÓN</label>
            <label for="radio2">REQUISITOS</label>
            <label for="radio3">DURACIÓN</label>
            <label for="radio4">PROFESOR</label>
            <label for="radio5">CONTENIDOS</label>

        </div>
        
        <div class="content">
           
            <input type="radio" name="radio" id="radio1" checked>
            <div class="tab1">
                <h2>Descripción</h2>
                <p>PHP es un lenguaje de código abierto muy popular especialmente adecuado para el desarrollo web y que puede ser incrustado en HTML.

                    Si tienes conocimientos de HTML y CSS habrás tenido ocasiones de comprobar que su principal limitación es la imposibilidad de crear Páginas Webs dinámicas, por el contrario no te preocupes. El siguiente paso en tu proceso de convertirte en todo un Profesional de la Programación Web y para ello debes aprender PHP.
                    
                    Comenzaremos desde lo básico, conociendo la historia del lenguaje y su estado actual, todo esto de forma fácil y amena. Con PHP se han hecho muchos sitios hoy día como lo son: Yahoo y Facebook
                    </p><br>

                <p>Curso en el cual te enseña a Desarrollar una Aplicación o Sitio Web, la cual podrás visualizar de forma Local o Remota (Accediendo desde cualquier parte del Mundo) y desde cualquier dispositivo móvil, tablet o Pc. Pon en práctica tus conocimientos básicos para así ir aumentando y facilitando la forma en la que programes.

                    Este Curso consta de 35 Videos explicados lo mas simple posible para que puedas analizar detenidamente y entender a Fondo el Lenguaje de Programación PHP con una Duración de 3 Horas.
                    
                    Iniciaremos con una breve Introducción de PHP y las Herramientas a utilizar para poder programar desde nuestro computador, Herramientas Totalmente Gratuitas, como lo son el XAMPP y Sublime Text.
                      </p> <br>
                  
                <p>Te daremos a conocer la Sintaxis Básica de PHP comenzando desde el: Hola Mundo!, Variables, Operaciones, Comentarios, Condicionales, Operadores con Condicionales, Funciones, Recoger Datos desde un Formulario, Bucles, Arrays y Crear una Calculadora que (Sume, Reste, Multiplique y Divida).

                    También te daré una Introducción a las Bases de Datos (MySQL), en la cual veremos como Agregar, Modificar, Eliminar y Consultar los Datos que tenemos en la misma.
                    
                    Estas mismas Funciones de Agregar, Modificar, Eliminar y Consultar las veremos también desde el Lenguaje de PHP para Conectarnos a nuestra Base de Datos (MySQL) y poder efectuar estas funciones.
                    </p>
                 <p>Finalmente te enseñaré a configurar tu Ordenador o PC para que puedas Visualizar tu Sitio web o Aplicaciones de Forma Local (En una Red Interna) o Forma Remota (Para que puedan Acceder desde Cualquier parte del Mundo) así como también desde Cualquier Dispositivo (Pc, Tablet, Móvil), para que puedas hacer las pruebas respectivas de tu Sitio Web o tus Aplicaciones.</p>
            </div>
            
            <input type="radio" name="radio" id="radio2">
            <div class="tab2">
                <h2>Requisitos</h2>
                <p>Saber utilizar un PC en un nivel Básico.</p>
                <p> Computadora personal con Windows.</p>
                <p> Tener Instalado un Navegador Web (Preferiblemente Chrome)</p>
                <p>300 Mbs de Espacio en Tu Disco Duro para la Instalación del (XAMPP y Sublime Text).       </p>
                    
            </div>
            
            <input type="radio" name="radio" id="radio3">
            <div class="tab3">
                <h2>Duración</h2>
                <p>Este curso dura 90h</p>
            </div>
            
            <input type="radio" name="radio" id="radio4">
            <div class="tab4">
                <h2>Roberto Terron Velo</h2>
                <p><img class="profe" src="./img/profe6.jpg" width="90" height="90"></p>
                 <p>Soy creador de múltiples proyectos en la web, donde cuento con mas de 300,000 estudiantes siguiendo mis cursos y tutoriales.

                    Me he dedicado al Diseño y Desarrollo web por mas de 9 años.</p>
            </div>

            <input type="radio" name="radio" id="radio5">
            <div class="tab5">
              <h2>Contenidos</h2>
              <p>Herramientas y Configuración a Utilizar 30h</p>
              <p>MySQL  30h</p>
               <p>PHP  30h </p>
            </div>
        </div>
    </div>
    <div class="container1"></div>
    <footer>
      <!-- Footer main -->
      <section class="ft-main">
       <div class="ft-main-item">
          <h2 class="ft-title">Regístrate</h2>
          <ul>
            <li><a href="registro.html">Regístrate</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Contacta con nosotros</h2>
          <ul>
            <li><a href="contactos.html">Contacto</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Suscríbete ahora</h2>
          <p>Suscríbete para no perderte nada</p>
          <form method="post">
            <input type="email" name="email" placeholder="Dirección email">
            <input type="submit" value="SUSCRIBETE">
          </form>
        </div>

        <div class="social">
            <a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
            <a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
            <a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
            <a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
       </div>
      </section>
    
    </footer>
 </html>