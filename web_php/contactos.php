<?php
include("config.php");

if(isset($_POST['contacto'])){

$nombre = $_POST['nombre'];
$apellido1 = $_POST['apellido1'];
$apellido2 = $_POST['apellido2'];
$email = $_POST['email'];
$asunto = $_POST['asunto'];
$mensaje = $_POST['mensaje'];
$tel = $_POST['tel'];



  
$sql="INSERT INTO contactos (nombre, primerapellido, segundoapellido, email , asunto, mensaje, telefono) VALUES ('$nombre', '$apellido1', '$apellido2', '$email', '$asunto', '$mensaje', '$tel')";

if (mysqli_query($conexion, $sql)) {
      echo "Mensaje enviado ";
} else {
      echo "Error: " . $sql . "<br>" . mysqli_error($conexion);
}
mysqli_close($conexion);
  }
?>


<!DOCTYPE html>
<html>
  <title>Contacto</title>
  <link rel="icon" href="./img/Logo.png">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>contactos.html</title>
  <link rel="stylesheet" type="text/css" href="./CSS/style.css">
  <link rel="stylesheet" type="text/css" href="./CSS/contactos.css">
</head>

<body>
  <h1><span>DADAFECH</span> Cursos</h1>

  <header>
    <a href="index.html"> <img class="logo" src="./img/Logo.png" width="70" height="80"></a>

    <nav>
        <ul> 
          <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
          <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
          <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
          <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
          <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
          <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
        </ul>
    </nav>
  </header>

    <br /><br />

    <p class="titulo">Contacta con Nosotros</p>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="contacto" >
      <div>
        <label>
          Nombre
          <input type="text" placeholder="Nombre" id="name" name="nombre" tabindex="1" size="20">
        </label>
      </div>
      <div>
        <label>
          Apellido 1
          <input type="text" placeholder="Apellidos" id="apellidos" name="apellido1" tabindex="2" size="55">
        </label>
      </div>
      <div>
        <label>
          Apellido 2
          <input type="text" placeholder="Apellidos" id="apellidos" name="apellido2" tabindex="2" size="55">
        </label>
      </div>
      <div>
        <label>
          Teléfono
          <input type="tel" placeholder="Teléfono" id="phone" name="tel" tabindex="3" size="18">
        </label>
      </div>
      <div>
        <label>
          Email
          <input type="email" placeholder="Email" id="email" name="email" tabindex="4" size="60" required="">
        </label>
      </div>
      <div>
        <label>
          Asunto
          <input type="text" placeholder="Asunto" id="asunto" name="asunto" tabindex="5">
        </label>
      </div>
      <div>
        <label>
          Mensaje
          <textarea id="skills" cols="55" rows="3" name="mensaje" tabindex="6"></textarea>
      </div>

      <div class="col-submit">
        <button class="submitbtn" name="contacto"> Enviar </button>
      </div>

    </form>

  </div>

  <footer>
    <!-- Footer main -->
    <section class="ft-main">
      <div class="ft-main-item">
          <h2 class="ft-title">Regístrate</h2>
          <ul>
            <li><a href="registro.html">Regístrate</a></li>
          </ul>
        </div>
      <div class="ft-main-item">
        <h2 class="ft-title">Contacta con nosotros</h2>
        <ul>
          <li><a href="contactos.html">Contacto</a></li>
        </ul>
      </div>

      <div class="ft-main-item">
        <h2 class="ft-title">Suscríbete ahora</h2>
        <p>Suscríbete para no perderte nada</p>        
          <input type="email" name="email" placeholder="Dirección email">
          <input type="submit" value="SUSCRIBETE">        
      </div>
    

      <div class="social">
        <a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
        <a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
        <a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
        <a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
      </div>
    </section>

  </footer>

</body>

</html>