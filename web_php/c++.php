<!DOCTYPE html>
<html>
<h1><span>DADAFECH</span> Cursos</h1>
<link rel="stylesheet" type="text/css" href="./CSS/modelo-curso.css">
<title>Curso C++</title>
<link rel="icon" href="./img/Logo.png">

<head>

  <meta charset="UTF-8">
</head>

<header>
     <a href="index.html"> <img class="logo" src="./img/Logo.png"
          width="70"
          height="80"></a>
     
     <nav>
     <ul> 
      <li><a href="index.php"><i class="icon-home"></i>Inicio</a></li>
      <li><a href="cursos.php"><i class="icon-user"></i>Cursos</a></li>
      <li><a href="contactos.php"><i class="icon-phone"></i>Contacto</a></li>
      <li><a href="FAQ.php"><i class="icon-login"></i>FAQS</a></li>
      <li><a href="registro.php"><i class="icon-register"></i>Regístrate</a></li>
      <li><a href="login.php"><i class="icon-login"></i>Login</a></li>
    </ul>
     
     </nav>
     </header>
<body>
    <div class="general">
        <div class="imagen">
            <img src="./img/c++.png" alt="" width="360" height="400">
            <a class="boton_personalizado" href="#">Agregar al carrito</a>

        </div>

        <div class="titulo">
            <h1> PROGRAMACIÓN EN C DE CERO A EXPERTO CON ESTRUCTURAS DE DATOS   </h1>
            <p>Lenguaje C con Algoritmos, Estructuras de Datos como Listas, Pilas, Colas. Arboles, Grafos Manejo de Archivos.</p> <br><br>
            <P>PRECIO: 130€ </P><br><br>
            <p><meter class="nivel" min="25" max="100"
              low="75" high="25"
              optimum="60" value="100"></meter> Nivel alto </p>
        </div>
    </div>
    <div class="container">
        <div class="lbl-menu">
            <label for="radio1">DESCRIPCIÓN</label>
            <label for="radio2">REQUISITOS</label>
            <label for="radio3">DURACIÓN</label>
            <label for="radio4">PROFESOR</label>
            <label for="radio5">CONTENIDOS</label>

        </div>
        
        <div class="content">
           
            <input type="radio" name="radio" id="radio1" checked>
            <div class="tab1">
                <h2>Descripción</h2>
                <p>Si lo que te interesa es practicar con ejercicios estas en el lugar correcto. Al pasar a los ejercicios aprenderemos buenas practicas y tips para programar de una manera mas eficiente que te servirán para cualquier lenguaje de programación posterior con la ayuda de Visual Studio Code Y CLion.
                </p><br>

                <p>¿Qué es C?  </p> <br>
                  
                <p>C es el lenguaje de programación de propósito general asociado, de modo universal, al sistema operativo UNIX. Sin embargo, la popularidad, eficacia y potencia de C, se ha producido porque este lenguaje no está prácticamente asociado a ningún sistema operativo, ni a ninguna máquina, en especial. Ésta es la razón fundamental, por la cual C, es conocido como el lenguaje de programación de sistemas, por excelencia.                </p> <br>
                  
                 <p>Ventajas de C:</p>
                 <p>El lenguaje C es poderoso y flexible, con órdenes, operaciones y funciones de biblioteca que se pueden utilizar para escribir la mayoría de los programas que corren en la computadora.  </p><br>
                  
                <p>C se utiliza por programadores profesionales para desarrollar software en la mayoría de los modernos sistemas de computadora.</p>   <br>
                <p>Se puede utilizar C para desarrollar sistemas operativos, compiladores, sistemas de tiempo real y aplicaciones de comunicaciones.       </p>
                <p>Un programa C puede ser escrito para un tipo de computadora y trasladarse a otra computadora con pocas o ninguna modificación</p>
                <p>En este curso Aprenderás:

                    Elementos Básicos
                    
                    Operaciones y Expresiones
                    
                    Estructuras de Selección
                    
                    Estructuras de Control con Bucles
                    
                    La librería String.h
                    
                    Funciones
                    
                    Arreglos con Listas y Tablas
                    
                    Estructuras
                    
                    Apuntadores
                    
                    Manejo de Archivos
                    
                    Cadenas
                    
                    Headers
                    
                    Proyecto (Contabilizar gastos)
                    
                    Memoria Estática vs Memoria Dinámica
                    
                    Asignación Dinámica de memoria
                    
                    Algoritmos de Ordenamiento
                    
                    Algoritmos de Búsqueda
                    
                    CLion
                    
                    Refuerzo Antes de Estructura de datos
                    
                    Introducción a las Estructuras de datos
                    
                    Estructuras de datos dinámicas Lineales
                    
                    Implementación de la Pila (usando Arreglos)
                    
                    Implementación de la Cola (usando Arreglos)
                    
                    Teoría de Listas
                    
                    Repaso Listas Ligadas
                    
                    Lista Simplemente Enlazada
                    
                    Lista Ligada Simplemente Enlazada
                    
                    Lista Ligada Circular
                    
                    Lista Ligada Doble
                    
                    Lista Ligada Circular Doble
                    
                    Pilas
                    
                    Colas
                    
                    Arboles
                    
                    Grafos
                    
                    Libros recomendados para fortalecer lo Aprendido
                    </p>
            </div>
            
            <input type="radio" name="radio" id="radio2">
            <div class="tab2">
                <h2>Requisitos</h2>
                <p>Contar con un ordenador de cualquier sistema operativo, de preferencia Linux, Mac OS o Windows</p>
                <p> No se requieren conocimientos previos de programación</p>
                   <p> Tener muchas ganas de aprender</p>
                    
            </div>
            
            <input type="radio" name="radio" id="radio3">
            <div class="tab3">
                <h2>Duración</h2>
                <p>Este curso dura 90h</p>
            </div>
            
            <input type="radio" name="radio" id="radio4">
            <div class="tab4">
                <h2>Cristobal Arcos Pérez</h2>
                <p><img class="profe" src="./img/profe4.jpg" width="90" height="90"></p>
                 <p> Hola, desde muy niño me llamo la atención todo lo que tenga que ver con programación, es así como me dediqué integramente a aprender programación de manera autodidácta.

                    Desde el 2012 me dedico a la enseñanza e investigación de la programación en todas sus áreas (escritorio, web, móvil, videojuegos).
                    
                    Dedicado a la permanente investigación, aprendizaje y enseñanza de el lenguaje JAVA.</p>
            </div>
            <input type="radio" name="radio" id="radio5">
            <div class="tab5">
              <h2>Contenidos</h2>
              <p>Elementos Básicos  30h</p>
              <p>Estructura de Selección IPv6  30h</p>
               <p>  Estructuras de Control con Bucles  30h </p>
            </div>
        </div>
    </div>
    <div class="container1"></div>
    <footer>
      <!-- Footer main -->
      <section class="ft-main">
        <div class="ft-main-item">
          <h2 class="ft-title">Regístrate</h2>
          <ul>
            <li><a href="registro.html">Regístrate</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Contacta con nosotros</h2>
          <ul>
            <li><a href="contactos.html">Contacto</a></li>
          </ul>
        </div>
        <div class="ft-main-item">
          <h2 class="ft-title">Suscríbete ahora</h2>
          <p>Suscríbete para no perderte nada</p>
          <form method="post">
            <input type="email" name="email" placeholder="Dirección email">
            <input type="submit" value="SUSCRIBETE">
          </form>
        </div>

        <div class="social">
            <a href="https://www.facebook.com/"><img src="./img/facebook.png" width="40" height="40" alt="facebook"></a>
            <a href="https://www.youtube.com/"><img src="./img/youtube.png" width="40" height="40" alt="youtube"></a>
            <a href="https://es.linkedin.com/"><img src="./img/linkedin.png" width="40" height="40" alt="linkedin"></a>
            <a href="https://www.instagram.com/"><img src="./img/instagram.png" width="40" height="40" alt="instagram"></a>
       </div>
      </section>
    
    </footer>
 </html>